var xhr = new XMLHttpRequest();
xhr.open('GET', 'http://localhost:3000/person', true);
xhr.onload = function(e) {
  if (xhr.readyState === 4) {
    if (xhr.status === 200) {
      let data = JSON.parse(xhr.responseText);

      document.getElementById('aboutMe').innerHTML = data.description;
      document.getElementById(
        'nameAndAge'
      ).innerHTML = `MY NAME IS ${data.name} ${data.age}YO AND THIS IS MY RESUME/CV`;

      var table = document.getElementById('education');
      var trTemplate = document.getElementById('template');

      for (let i = 0; i < data.educations.length; i++) {
        let newTr;
        if (i != 0) newTr = trTemplate.cloneNode(true);
        else newTr = trTemplate;
        newTr.firstElementChild.innerHTML = data.educations[i].year;
        newTr.lastElementChild.firstElementChild.lastElementChild.innerHTML =
          data.educations[i].description;
        newTr.lastElementChild.firstElementChild.firstElementChild.innerHTML =
          data.educations[i].title;
        if (i != 0) table.appendChild(newTr);
      }
    } else {
      console.error(xhr.statusText);
    }
  }
};
xhr.onerror = function(e) {
  console.error(xhr.statusText);
};
xhr.send(null);
